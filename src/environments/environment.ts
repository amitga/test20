// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
   firebaseConfig: {
    apiKey: "AIzaSyBGGwh7gebypTwpQaF3K6KHSmexB9drpdI",
    authDomain: "test20-5fa0d.firebaseapp.com",
    databaseURL: "https://test20-5fa0d.firebaseio.com",
    projectId: "test20-5fa0d",
    storageBucket: "test20-5fa0d.appspot.com",
    messagingSenderId: "465147402563",
    appId: "1:465147402563:web:ce5690824e68f6273ab7f6",
    measurementId: "G-XPKSD2KNZ8"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
