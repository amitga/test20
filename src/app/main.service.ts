import { Main } from './interfaces/main';
import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class MainService {

  apiUrl='https://jsonplaceholder.typicode.com/posts/';

  constructor(private http: HttpClient, private db: AngularFirestore) { }

/* GETING FROM JSON*/
getMains(){
  return this.http.get<Main[]>(this.apiUrl)
}

}
