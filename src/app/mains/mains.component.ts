import { Component, OnInit } from '@angular/core';
import { Main } from '../interfaces/main';
import { MainService } from '../main.service';

@Component({
  selector: 'app-mains',
  templateUrl: './mains.component.html',
  styleUrls: ['./mains.component.css']
})
export class MainsComponent implements OnInit {

  //geting posts from json not from db
  mains$: Main[];

  constructor(private mainService: MainService) { }
  ngOnInit() {
    //geting posts from json
        this.mainService.getMains().subscribe(data => this.mains$ = data)

  }

}
