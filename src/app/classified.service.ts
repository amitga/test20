import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ClassifiedService {
  classify() {
    throw new Error("Method not implemented.");
  }
  
  constructor(private db: AngularFirestore,private router:Router) {}

  addArticle(category:String, body:String, img:string){
    const article = {category:category, body:body, img:img}
    this.db.collection('articles').add(article)  
    this.router.navigate(['/articles']);
  }

  //get from db
  getArticles():Observable<any[]>{
  return this.db.collection('articles').valueChanges({idField:'id'});
  }

  deleteArticle(id:string){
    this.db.doc(`articles/${id}`).delete();
  }
}
