import { TestBed } from '@angular/core/testing';

import { GeneralsService } from './generals.service';

describe('GeneralsService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: GeneralsService = TestBed.get(GeneralsService);
    expect(service).toBeTruthy();
  });
});
